/**
 * Reservoir Sampling
 *
 * This algorithm is useful for randomly choosing N elements from an unknown
 * sized stream of inputs with an equal distribution.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

enum 
{
	MAX_READ_BUFFER = 256
};

static inline char *
CopyString(char *String)
{
	size_t StringLength = strlen(String) + 1;
	char *Result = (char *)malloc(sizeof(*Result)*StringLength);
	assert(Result);
	strncpy(Result, String, StringLength);

	return(Result);
}

static void
DeleteString(char **String)
{
	assert(*String);
	free(*String);
	*String = 0;
}

int main(int32_t ArgCount, char **Args)
{
	srand(time(0));

	if(ArgCount != 2)
	{
		printf("Usage: %s [count]\n", Args[0]);
		return 1;
	}

	int32_t ResultCount = atoi(Args[1]);
	if(ResultCount < 0)
	{
		fprintf(stderr, "Count must be a positive number.\n");
		return 2;
	}

	char **Results = (char **)malloc(sizeof(*Results) * ResultCount);
	assert(Results != 0);

	int32_t InputCount = 0;
	char ReadBuffer[MAX_READ_BUFFER] = {};
	for(int32_t Index = 0; Index < ResultCount; ++Index, ++InputCount)
	{
		if(fgets(ReadBuffer, MAX_READ_BUFFER, stdin) == 0)
		{
			break;
		}

		Results[Index] = CopyString(ReadBuffer);
	}

	while((fgets(ReadBuffer, MAX_READ_BUFFER, stdin)) != 0)
	{
		++InputCount;

		int32_t RandomIndex = rand() % InputCount;
		if(RandomIndex < ResultCount)
		{
			DeleteString(&Results[RandomIndex]);
			Results[RandomIndex] = CopyString(ReadBuffer);
		}
	}

	printf("\n");
	for(int32_t Index = 0; Index < ResultCount; ++Index)
	{
		printf("%s", Results[Index]);
		DeleteString(&Results[Index]);
	}
	free(Results);

	return 0;
}

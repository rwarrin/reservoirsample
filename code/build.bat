@echo off

REM set CompilerFlags=/Oi /Od /Z7 /fp:fast /WX /W4 /wd4996 /wd4244
set CompilerFlags=/Ox /Oi /favor:INTEL64 /fp:fast /WX /W4 /FeReservoirSample /wd4996 /wd4244

IF NOT EXIST ..\build mkdir ..\build

pushd ..\build

cl /nologo %CompilerFlags% ../code/main.cpp /link /incremental:no /opt:ref

popd
